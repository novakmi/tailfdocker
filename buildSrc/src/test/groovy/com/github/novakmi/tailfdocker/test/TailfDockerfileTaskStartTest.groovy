package com.github.novakmi.tailfdocker.test

import com.github.novakmi.tailfdocker.TailfDockerfileTask
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.testng.Assert
import org.testng.annotations.Test

class TailfDockerfileTaskStartTest {

    @Test(groups=["basic"])
    public void canAddTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        def task = project.task('tailfdocker', type: TailfDockerfileTask)
        Assert.assertTrue(task instanceof TailfDockerfileTask)
    }

}
