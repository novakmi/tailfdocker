package com.github.novakmi.tailfdocker


import org.gradle.api.Project
import org.gradle.api.Plugin


class TailfDockerPlugin implements Plugin<Project> {
    //@Override
    /**
     * Gradle plugin entry point.
     * Registers plugin ` LxDockerfile` of `LxDockerfileTask` type.
     *
     * @param target
     * @see TailfDockerfileTask
     */
    void apply(Project target) {
        target.task('TailfDockerfile', type: TailfDockerfileTask)
    }
}
